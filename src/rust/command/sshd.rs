/* src/rust/command/sshd.rs
 * ========================
 *
 * Copying
 * -------
 *
 * Copyright (c) 2020 Zyn authors and contributors.
 *
 * This file is part of the *Zyn* project.
 *
 * Zyn is a free software project. You can redistribute it and/or
 * modify it following the terms of the MIT License.
 *
 * This software project is distributed *as is*, WITHOUT WARRANTY OF ANY
 * KIND; including but not limited to the WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE and NONINFRINGEMENT.
 *
 * You should have received a copy of the MIT License along with
 * *Zyn*. If not, see <http://opensource.org/licenses/MIT>.
 */
#![warn(missing_docs)]

//! # zyn::command::sshd
//!
//! Execution facilities for the `zyn-sshd` service.

use std::convert::TryFrom;
use std::fs::File;
use std::net::{TcpListener, TcpStream};
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::sync::Arc;

use log::*;
use structopt::StructOpt;

use privdrop::PrivDrop;

use nix::unistd::{fork, ForkResult};

use thrussh_keys::key::KeyPair;
use thrussh_keys::key::SignatureHash;

use crate::data::config::{Config, SSHd};
use crate::errors::*;
use crate::result::*;

/// Name of the location where `zyn-sshd` is keeping its private data.
const ZYN_SSHD_PRIVATE: &'static str = "private";
/// The beginning part of a `zyn-sshd` host key.
const ZYN_SSHD_HOST_KEY_PREFIX: &'static str = "ssh_host_";
/// The end part of a `zyn-sshd` host key.
const ZYN_SSHD_HOST_KEY_SUFFIX: &'static str = "_key";
/// The end part of a `zyn-sshd` public host key.
const ZYN_SSHD_PUBLIC_KEY_EXT: &'static str = "pub";

/// The permissions mask to ensure private directory privacy.
const ZYN_SSHD_PRIVATE_MODE_MASK: u32 = 0o7737077;
/// The permissions mask to ensure private keys privacy.
const ZYN_SSHD_KEY_MODE_MASK: u32 = 0o7677377;
/// All bits set to `1` for the unix mode.
const UNIX_FS_MODE_FULL_BITS: u32 = 0o7777777;

/// The default number of bits in the RSA keys.
const ZYN_SSHD_KEY_RSA_BITS_DEFAULT: usize = 3072;
/// The default signature used for the RSA keys.
const ZYN_SSHD_KEY_RSA_SIGNATURE_DEFAULT: SignatureHash =
    SignatureHash::SHA2_256;

/// The maximum size of an IP packet.
const INET_MAX_PACKET_SIZE: u32 = 65535;

/// Check the access rights on the `private` folder to ensure correct privacy.
///
///
/// # Arguments
///
/// * `base` - Path to the directory where `zyn-sshd` is keeping its state data.
///
///
/// # Returns
///
/// Whether or not privacy is ensured.
fn sshd_ensure_private(base: &Path) -> Result<()> {
    let private = base.join(format!("{}", ZYN_SSHD_PRIVATE));
    debug!("Using private folder at: {}.", private.to_str().unwrap());

    if private.is_dir() {
        let mode = private.metadata()?.permissions().mode();
        if (mode & ZYN_SSHD_PRIVATE_MODE_MASK) != 0 {
            debug!(
                "Private folder permissions: {} ({:o}).",
                private.to_str().unwrap(),
                mode
            );

            return Err(ZynIoError::FileModeInvalid {
                location: private.to_str().unwrap().into(),
                mode,
            }
            .into());
        }

        if let Ok(entries) = private.read_dir() {
            for entry in entries {
                if let Ok(entry) = entry {
                    let fname = entry.file_name().into_string().unwrap();
                    let fpath = entry.path();

                    if !(fname.starts_with(ZYN_SSHD_HOST_KEY_PREFIX)
                        && fname.ends_with(ZYN_SSHD_HOST_KEY_SUFFIX))
                    {
                        trace!(
                            "Skipping directory entry: {}.",
                            fpath.to_str().unwrap()
                        );
                        continue;
                    }

                    let mode = entry.metadata()?.permissions().mode();
                    if (mode & ZYN_SSHD_KEY_MODE_MASK) != 0 {
                        debug!(
                            "Host key permissions: {} ({:o}).",
                            fpath.to_str().unwrap(),
                            mode
                        );

                        return Err(ZynIoError::FileModeInvalid {
                            location: fpath.to_str().unwrap().into(),
                            mode,
                        }
                        .into());
                    }
                }
            }
        }
    } else {
        return Err(
            ZynIoError::FileNotFound(private.to_str().unwrap().into()).into()
        );
    }

    Ok(())
}

/// Create the host keys for `zyn-sshd`.
///
///
/// # Arguments
///
/// * `base` - Path to the directory where `zyn-sshd` is keeping its state data.
/// * `force` - Whether or not to force the creation of new keys even if
///   existing ones are found.
///
/// # Returns
///
/// Whether or not the keys could successfully be created.
fn sshd_host_keys_create(base: &Path, force: bool) -> Result<()> {
    let private = base.join(format!("{}", ZYN_SSHD_PRIVATE));
    debug!("Using private folder at: {}.", private.to_str().unwrap());

    std::fs::create_dir_all(&private).and_then(|()| {
        trace!(
            "Creating private folder with mask: 0o{:o}.",
            ZYN_SSHD_PRIVATE_MODE_MASK
        );
        std::fs::set_permissions(
            &private,
            std::fs::Permissions::from_mode(
                UNIX_FS_MODE_FULL_BITS ^ ZYN_SSHD_PRIVATE_MODE_MASK,
            ),
        )?;

        Ok(())
    })?;

    let keys: Vec<KeyPair> = vec![
        KeyPair::generate_rsa(
            ZYN_SSHD_KEY_RSA_BITS_DEFAULT,
            ZYN_SSHD_KEY_RSA_SIGNATURE_DEFAULT,
        )
        .unwrap(),
        KeyPair::generate_ed25519().unwrap(),
    ];
    for k in keys {
        info!("Generating host key type: {}.", k.name());
        let k_name = format!(
            "{}{}{}",
            ZYN_SSHD_HOST_KEY_PREFIX,
            k.name().replace("-", "_"),
            ZYN_SSHD_HOST_KEY_SUFFIX
        );
        let k_path = private.join(k_name);
        let kpub_path = k_path.with_extension(ZYN_SSHD_PUBLIC_KEY_EXT);
        if k_path.exists() {
            let k_size = File::open(&k_path)?.metadata()?.len();
            debug!(
                "Size of existing key at: {} ({}).",
                k_path.to_str().unwrap(),
                k_size
            );
            if k_size != 0 && !force {
                // There is already an existing host private key for this key type.
                info!(
                    "Skipping existing host key: {}.",
                    k_path.to_str().unwrap()
                );
                continue;
            }
        }
        debug!("Generating host key at: {}.", k_path.to_str().unwrap());

        let k_tmp = mktemp::Temp::new_file_in(&private)?;
        trace!("Host key temporary file: {}.", k_tmp.to_str().unwrap());
        let kpub_tmp = mktemp::Temp::new_file_in(&private)?;
        trace!(
            "Host public key temporary file: {}.",
            kpub_tmp.to_str().unwrap()
        );

        trace!(
            "Writing temporary private key at: {}.",
            k_tmp.to_str().unwrap()
        );
        let k_fp = File::create(&k_tmp)?;
        thrussh_keys::encode_pkcs8_pem(&k, &k_fp)?;
        trace!("Calling sync on: {}.", k_tmp.to_str().unwrap());
        k_fp.sync_all()?;
        trace!(
            "Installing private key: {}{}{{{} -> {}}}.",
            k_tmp.parent().unwrap().to_str().unwrap(),
            std::path::MAIN_SEPARATOR,
            k_tmp.file_name().unwrap().to_str().unwrap(),
            k_path.file_name().unwrap().to_str().unwrap()
        );
        std::fs::copy(&k_tmp, &k_path)?;

        trace!(
            "Setting private key permissions with mask: 0o{:o}.",
            ZYN_SSHD_KEY_MODE_MASK
        );
        std::fs::set_permissions(
            &k_path,
            std::fs::Permissions::from_mode(
                UNIX_FS_MODE_FULL_BITS ^ ZYN_SSHD_KEY_MODE_MASK,
            ),
        )?;

        trace!(
            "Writing temporary public key at: {}.",
            kpub_tmp.to_str().unwrap()
        );
        let kpub_fp = File::create(&kpub_tmp)?;
        thrussh_keys::write_public_key_base64(&kpub_fp, &k.clone_public_key())?;
        trace!("Calling sync on: {}.", kpub_tmp.to_str().unwrap());
        kpub_fp.sync_all()?;
        trace!(
            "Installing private key: {}{}{{{} -> {}}}.",
            kpub_tmp.parent().unwrap().to_str().unwrap(),
            std::path::MAIN_SEPARATOR,
            kpub_tmp.file_name().unwrap().to_str().unwrap(),
            kpub_path.file_name().unwrap().to_str().unwrap()
        );
        std::fs::copy(&kpub_tmp, &kpub_path)?;
    }

    Ok(())
}

/// Load the host keys stored on the filesystem.
///
///
/// # Arguments
///
/// * `base` - Path to the directory where `zyn-sshd` is keeping its state data.
///
///
/// # Returns
///
/// Return a vector of keys loaded from the filesystem.
fn sshd_host_keys_read(base: &Path) -> Result<Vec<KeyPair>> {
    sshd_ensure_private(&base)?;
    let private = base.join(format!("{}", ZYN_SSHD_PRIVATE));
    debug!("Using private folder at: {}.", private.to_str().unwrap());

    let mut keys: Vec<KeyPair> = vec![];
    if let Ok(entries) = private.read_dir() {
        for entry in entries {
            if let Ok(entry) = entry {
                let fname = entry.file_name().into_string().unwrap();
                if !(fname.starts_with(ZYN_SSHD_HOST_KEY_PREFIX)
                    && fname.ends_with(ZYN_SSHD_HOST_KEY_SUFFIX))
                {
                    trace!("Skipping directory entry: {}.", fname);
                    continue;
                }
                debug!(
                    "Loading host key at: {}.",
                    entry.path().to_str().unwrap()
                );
                keys.push(thrussh_keys::load_secret_key(entry.path(), None)?);
            }
        }
    }

    Ok(keys)
}

/// Generate the configuration expected by [thrussh::server::Server].
///
///
/// # Arguments
///
/// * `config` - The final Zyn configuration the application should execute
///              with.
///
///
/// # Returns
///
/// A [thrussh::server::Config] object which values are populated from provided
/// Zyn configuration.
fn ssh_server_config(config: &Config) -> Result<thrussh::server::Config> {
    let mut cfg = thrussh::server::Config::default();

    let default_state = SSHd::default_state().unwrap();
    let state =
        Path::new(config.zyn.sshd.state.as_ref().unwrap_or(&default_state));

    let mut keys =
        sshd_host_keys_read(state).or_else(|_| -> Result<Vec<KeyPair>> {
            sshd_host_keys_create(state, false)?;
            sshd_host_keys_read(state)
        })?;
    cfg.keys.append(&mut keys);

    cfg.connection_timeout = Some(
        config
            .zyn
            .sshd
            .timeout
            .clone()
            .unwrap_or(SSHd::default().timeout.unwrap()),
    );
    cfg.max_auth_attempts = config
        .zyn
        .sshd
        .authtries
        .clone()
        .unwrap_or(SSHd::default().authtries.unwrap());
    cfg.methods = thrussh::MethodSet::PUBLICKEY;
    cfg.preferred = thrussh::Preferred::COMPRESSED;
    cfg.server_id = format!(
        "SSH-2.0-{}_v{}",
        env!("CARGO_BIN_NAME"),
        env!("CARGO_PKG_VERSION")
    );

    Ok(cfg)
}

/// Generate a new server to handle the incoming SSH connection and detach the
/// handler from the daemon process.
///
///
/// # Arguments
///
/// * `stream` - The SSH connection to be handled.
/// * `server_cfg` - Configuration statements specific to the handling of the
///   SSH connection.
/// * `zyn_cfg` - The final Zyn configuration the application should run with.
///
///
/// # Returns
///
/// This function returns any error which occurred during its operation.
fn spawn_server(
    stream: TcpStream,
    server_cfg: Arc<thrussh::server::Config>,
    zyn_cfg: Arc<Config>,
) -> Result<()> {
    if nix::unistd::geteuid().is_root() {
        let default = SSHd::default();
        let group = zyn_cfg
            .zyn
            .sshd
            .group
            .clone()
            .unwrap_or(default.group.unwrap());
        let user = zyn_cfg
            .zyn
            .sshd
            .user
            .clone()
            .unwrap_or(default.user.unwrap());

        trace!("Dropping privileges to {}:{}.", user, group);
        // TODO: Chroot to the directory hosting the Git repositories.
        if let Err(e) = PrivDrop::default().user(&user).group(&group).apply() {
            error!("{}", e);
            panic!("Could not drop privileges to user {:?}.", user);
        }
    }

    unsafe {
        match fork()? {
            ForkResult::Parent { child, .. } => {
                trace!(
                    "Server handler for {}: {:?}.",
                    stream.peer_addr()?,
                    child
                );
                return Ok(());
            },
            ForkResult::Child => server_main(stream, server_cfg, zyn_cfg),
        }
    }
}

/// Entry point of the asynchronous SSH server handler.
///
///
/// # Arguments
///
/// * `stream` - The SSH connection to be handled.
/// * `server_cfg` - Configuration statements specific to the handling of the
///   SSH connection.
/// * `zyn_cfg` - The final Zyn configuration the application should run with.
///
///
/// # Returns
///
/// This function returns any error which occurred during its operation.
#[tokio::main]
async fn server_main(
    stream: TcpStream,
    server_cfg: Arc<thrussh::server::Config>,
    zyn_cfg: Arc<Config>,
) -> Result<()> {
    let stream = tokio::net::TcpStream::from_std(stream)?;
    let sshd = zyn::ssh::server::Server::new(zyn_cfg, stream.peer_addr().ok());

    trace!(
        "{} - Handling connection from {:?}.",
        sshd.id,
        stream.peer_addr()?
    );
    tokio::spawn(thrussh::server::run_stream(server_cfg, stream, sshd))
        .await??;
    Ok(())
}

/// Execution loop of the `zyn-sshd` binary.
///
///
/// # Arguments
///
/// * `config` - The final Zyn configuration the application should execute
///              with.
///
///
/// # Returns
///
/// This function returns any error which occurred during its operation.
pub fn execute(config: Config) -> Result<()> {
    debug!("Run configuration: {:?}", config);
    let config = Arc::new(config);

    let mut server_cfg = ssh_server_config(&config)?;
    if server_cfg.maximum_packet_size > INET_MAX_PACKET_SIZE {
        warn!(
            "Maximum packet size ({:?}) is too large. Setting to {:?}.",
            server_cfg.maximum_packet_size, INET_MAX_PACKET_SIZE
        );
        server_cfg.maximum_packet_size = INET_MAX_PACKET_SIZE;
    }
    debug!("SSH server configuration: {:?}", server_cfg);
    let server_cfg = Arc::new(server_cfg);

    let listen = config
        .zyn
        .sshd
        .listen
        .clone()
        .unwrap_or(SSHd::default_listen().unwrap());
    info!("Listening on {}.", listen);

    let socket = TcpListener::bind(&listen)?;
    for stream in socket.incoming() {
        if let Ok(stream) = stream {
            match stream.peer_addr() {
                Ok(peer) => {
                    trace!("New connection from {}.", peer);
                    unsafe {
                        match fork()? {
                            ForkResult::Parent { child, .. } => {
                                trace!(
                                    "Waiting for server to spawn: {:?}.",
                                    child
                                );
                                let _ = nix::sys::wait::waitpid(child, None);
                            },
                            ForkResult::Child => {
                                return spawn_server(
                                    stream,
                                    server_cfg.clone(),
                                    config.clone(),
                                );
                            },
                        }
                    }
                },
                Err(e) => {
                    error!("Could not handle connection: {:?}", e);
                },
            }
        } else {
            error!("Could not handle connection: {:?}", stream);
        }
    }
    Ok(())
}
