/* src/rust/bin/zyn-sshd.rs
 * ========================
 *
 * Copying
 * -------
 *
 * Copyright (c) 2020 Zyn authors and contributors.
 *
 * This file is part of the *Zyn* project.
 *
 * Zyn is a free software project. You can redistribute it and/or
 * modify it following the terms of the MIT License.
 *
 * This software project is distributed *as is*, WITHOUT WARRANTY OF ANY
 * KIND; including but not limited to the WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE and NONINFRINGEMENT.
 *
 * You should have received a copy of the MIT License along with
 * *Zyn*. If not, see <http://opensource.org/licenses/MIT>.
 */
#![warn(missing_docs)]

//! # zyn-sshd
//!
//! `zyn-sshd` is a service from the Zyn project which provides an SSH server
//! to manage a Git repository.

use zyn::data::config::{Config, SSHd};
use zyn::result::*;

/// Entry point of the `zyn-sshd` binary.
fn main() -> Result<()> {
    let opt: SSHd = SSHd::from_args();
    let config = match opt
        .config
        .as_ref()
        .map_or(Ok(Config::default()), |x| Config::try_from(x.as_ref()))
    {
        Ok(mut x) => {
            x.zyn.sshd = opt;
            x
        },
        Err(e) => {
            let default = Config::default();
            zyn::log::stderrlogger(
                module_path!(),
                default.zyn.quiet,
                default.zyn.verbose,
            );
            return e.into();
        },
    };

    let quiet = config.zyn.sshd.quiet || config.zyn.quiet;
    let verbosity: usize = match config.zyn.sshd.verbose {
        0 => config.zyn.verbose,
        _ => config.zyn.sshd.verbose,
    };
    zyn::log::stderrlogger(module_path!(), quiet, verbosity);

    zyn::command::sshd::execute(config)
}
