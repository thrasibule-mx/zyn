/* src/rust/bin/zyn.rs
 * ===================
 *
 * Copying
 * -------
 *
 * Copyright (c) 2020 Zyn authors and contributors.
 *
 * This file is part of the *Zyn* project.
 *
 * Zyn is a free software project. You can redistribute it and/or
 * modify it following the terms of the MIT License.
 *
 * This software project is distributed *as is*, WITHOUT WARRANTY OF ANY
 * KIND; including but not limited to the WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE and NONINFRINGEMENT.
 *
 * You should have received a copy of the MIT License along with
 * *Zyn*. If not, see <http://opensource.org/licenses/MIT>.
 */
#![warn(missing_docs)]

//! # zyn
//!
//! `zyn` is the entry user interface to manage a Zyn site.

use log;

use structopt::StructOpt;

use zyn::data::config::{Config, Zyn, ZynCommand};
use zyn::result::*;

/// Execution loop of the `zyn` binary.
///
///
/// # Arguments
///
/// * `config` - The final Zyn configuration the application should execute
///              with.
///
///
/// # Returns
///
/// This function returns any error which occurred during its operation.
fn run(config: Config) -> Result<()> {
    log::debug!("Run configuration: {:?}", config);
    match config.zyn.command {
        #[cfg(feature = "sshd")]
        Some(ZynCommand::SSHD(..)) => zyn::command::sshd::execute(config),
        None => todo!(),
    }
}

/// Entry point of the `zyn` binary.
fn main() -> Result<()> {
    let mut config = Config::default();
    config.zyn = Zyn::from_args();

    zyn::log::stderrlogger(
        module_path!(),
        config.zyn.quiet,
        config.zyn.verbose,
    );
    run(config)
}
