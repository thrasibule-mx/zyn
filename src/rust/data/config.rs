/* src/rust/data/config.rs
 * =======================
 *
 * Copying
 * -------
 *
 * Copyright (c) 2020 Zyn authors and contributors.
 *
 * This file is part of the *Zyn* project.
 *
 * Zyn is a free software project. You can redistribute it and/or
 * modify it following the terms of the MIT License.
 *
 * This software project is distributed *as is*, WITHOUT WARRANTY OF ANY
 * KIND; including but not limited to the WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE and NONINFRINGEMENT.
 *
 * You should have received a copy of the MIT License along with
 * *Zyn*. If not, see <http://opensource.org/licenses/MIT>.
 */

//! # zyn::data::config
//!
//! Definition of the data structures allowing the user to configure various
//! aspects of the project.
//!
//! A typical configuration can be provided as a file in [TOML](https://toml.io/)
//! format however any other backend can supported as long as it results in the
//! common [Config] structure.
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::net::{IpAddr, Ipv6Addr, SocketAddr};
use std::convert::TryFrom;

use serde::Deserialize;
use structopt::StructOpt;
use humantime_serde::re::humantime;

use crate::errors::*;


/// Name of the environment variable to fetch the configuration from.
pub const ENV_ZYN_CONFIG: &'static str = "ZYN_CONFIG";
/// Name of the environment variable specifying to `zyn-sshd` the system group
/// to drop privileges to.
pub const ENV_ZYN_SSHD_GROUP: &'static str = "ZYN_SSHD_GROUP";
/// Name of the environment variable instructing to `zyn-sshd` where to keep
/// its data.
pub const ENV_ZYN_SSHD_STATE: &'static str = "ZYN_SSHD_STATE";
/// Name of the environment variable specifying to `zyn-sshd` the system user
/// to drop privileges to.
pub const ENV_ZYN_SSHD_USER: &'static str = "ZYN_SSHD_USER";

/// Short name given to `zyn-sshd`.
pub const ZYN_SSHD_SHORT_NAME: &'static str = "sshd";

/// Default number of authentication attempts `zyn-sshd` will allow per
/// connection.
const ZYN_SSHD_AUTHTRIES_DEFAULT: usize = 6;
/// Default system group for `zyn-sshd` to drop privileges to when running with
/// administrative rights.
const ZYN_SSHD_GROUP_DEFAULT: &'static str = "zyn";
/// Default system user for `zyn-sshd` to drop privileges to when running with
/// administrative rights.
const ZYN_SSHD_USER_DEFAULT: &'static str = "zyn";
/// Default duration in seconds for `zyn-sshd` to wait for data from the
/// client.
const ZYN_SSHD_TIMEOUT_DEFAULT: u64 = 15;


/// Zyn command line interface.
#[derive(Debug, Deserialize, StructOpt)]
pub enum ZynCommand {
    /// Start the SSH service.
    #[cfg(feature = "sshd")]
    SSHD(SSHd),
}

/// Overall Zyn configuration data structure.
#[derive(Debug, Deserialize)]
#[serde(default)]
pub struct Config {
    /// All configuration statements are children of the `zyn` key.
    pub zyn: Zyn,
}

impl Config {
    /// Provide the default location to load the configuration from.
    pub fn default_location() -> Option<String> {
        // So far, looking for the `${SYSCONFDIR}/zyn/zyn.toml` configuration
        // file.
        Path::new(env!("SYSCONFDIR"))
            .join(env!("CARGO_PKG_NAME"))
            .join(env!("CARGO_PKG_NAME"))
            .with_extension("toml")
            .to_str()
            .map_or(None, |x| { Some(String::from(x)) })
    }
}

impl Default for Config {
    /// Provide a [Config] structure with its default values.
    fn default() -> Self {
        Self {
            zyn: Zyn::default(),
        }
    }
}

impl TryFrom<&str> for Config {
    type Error = ZynIoError;

    /// Load a configuration from given string value.
    ///
    ///
    /// # Arguments
    ///
    /// * `value` - The string value to load the configuration from this could
    ///             be a path or an URL.
    ///
    ///
    /// # Returns
    ///
    /// A [Result] object with the successfully loaded configuration or any
    /// raised error.
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut content = String::new();
        File::open(Path::new(value))
            .and_then(|mut fp| fp.read_to_string(&mut content))
            .map_err(|e| ZynIoError::FileOperation(e))?;

        toml::from_str::<Self>(&content).map_err(|e| ZynIoError::ConfigLoad(e))
    }
}

/// All configuration options for `zyn` and other support services.
#[derive(Debug, Deserialize, StructOpt)]
#[structopt(
    name = "zyn",
    about = "The main command to build and manage your Zyn site"
)]
pub struct Zyn {
    /// The action to be performed on the site.
    #[structopt(subcommand)]
    pub command: Option<ZynCommand>,
    /// Silence all informative output
    #[structopt(short, long)]
    pub quiet: bool,
    /// Verbosity level, the more the merrier
    #[structopt(short, long, parse(from_occurrences))]
    pub verbose: usize,
}

impl Default for Zyn {
    /// Provide a [Zyn] structure with its default values.
    fn default() -> Self {
        Self {
            command: None,
            quiet: false,
            verbose: 0,
        }
    }
}


/// Configuration options specific to the `zyn-sshd` service.
#[derive(Debug, Deserialize, StructOpt)]
#[structopt(
    name = "zyn-sshd",
    about = "A Zyn service for managing a local repository over the SSH protocol."
)]
pub struct SSHd {
    /// Maximum number of authentication attempts permitted per connection.
    #[structopt(short = "r", long)]
    pub authtries: Option<usize>,
    /// Path or URL to the configuration.
    #[structopt(short, long, env = ENV_ZYN_CONFIG)]
    pub config: Option<String>,
    /// The system group to which to drop privileges to when running with
    /// administrative rights.
    #[structopt(short, long, env = ENV_ZYN_SSHD_GROUP)]
    pub group: Option<String>,
    /// The address the server should listen on.
    #[structopt(short = "L", long)]
    pub listen: Option<SocketAddr>,
    /// Silence all informative output
    #[structopt(short, long)]
    pub quiet: bool,
    /// Location where `zyn-sshd` is keeping its run data.
    #[structopt(short, long, env = ENV_ZYN_SSHD_STATE)]
    pub state: Option<String>,
    /// The maximum duration to wait for data from the client.
    #[structopt(short, long, parse(try_from_str = humantime::parse_duration))]
    #[serde(with = "humantime_serde")]
    pub timeout: Option<std::time::Duration>,
    /// The system user to which to drop privileges to when running with
    /// administrative rights.
    #[structopt(short, long, env = ENV_ZYN_SSHD_USER)]
    pub user: Option<String>,
    /// Verbosity level, the more the merrier
    #[structopt(short, long, parse(from_occurrences))]
    pub verbose: usize,
}

impl SSHd {
    /// Provide the default address the server should listen on.
    pub fn default_listen() -> Option<SocketAddr> {
        Some(SocketAddr::new(
            IpAddr::V6(Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0)),
            22,
        ))
    }

    /// Provide the default location of `zyn-sshd` state data.
    pub fn default_state() -> Option<String> {
        // So far, looking for the `${SYSCONFDIR}/zyn/sshd/` folder.
        Path::new(env!("LOCALSTATEDIR"))
            .join(env!("CARGO_PKG_NAME"))
            .join(format!("{}", ZYN_SSHD_SHORT_NAME))
            .to_str()
            .map_or(None, |x| Some(String::from(x)))
    }
}

impl Default for SSHd {
    /// Generate the default values for the command line options.
    fn default() -> Self {
        Self {
            authtries: Some(ZYN_SSHD_AUTHTRIES_DEFAULT),
            // TODO: Ensure specific binary configuration location.
            config: Config::default_location(),
            group: Some(String::from(ZYN_SSHD_GROUP_DEFAULT)),
            listen: SSHd::default_listen(),
            quiet: false,
            state: SSHd::default_state(),
            timeout: Some(std::time::Duration::from_secs(
                ZYN_SSHD_TIMEOUT_DEFAULT,
            )),
            user: Some(String::from(ZYN_SSHD_USER_DEFAULT)),
            verbose: 0,
        }
    }
}
