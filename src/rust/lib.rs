/* src/rust/lib.rs
 * ===============
 *
 * Copying
 * -------
 *
 * Copyright (c) 2020 Zyn authors and contributors.
 *
 * This file is part of the *Zyn* project.
 *
 * Zyn is a free software project. You can redistribute it and/or
 * modify it following the terms of the MIT License.
 *
 * This software project is distributed *as is*, WITHOUT WARRANTY OF ANY
 * KIND; including but not limited to the WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE and NONINFRINGEMENT.
 *
 * You should have received a copy of the MIT License along with
 * *Zyn*. If not, see <http://opensource.org/licenses/MIT>.
 */
#![warn(missing_docs)]

//! # zyn
//!
//! `zyn` is the library supporting the overall features provided by the
//! project.

#[cfg(any(feature = "bin", feature = "lib"))]
pub mod errors;

#[cfg(feature = "bin")]
pub mod log;
#[cfg(feature = "bin")]
pub mod result;

#[cfg(feature = "bin")]
pub mod command;

#[cfg(feature = "config")]
pub mod data;
#[cfg(feature = "ssh-server")]
pub mod ssh;

/// Replicate the version of the package as provided by Cargo.
pub const VERSION_STR: &'static str = env!("CARGO_PKG_VERSION");
