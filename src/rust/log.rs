/* src/rust/log.rs
 * ===============
 *
 * Copying
 * -------
 *
 * Copyright (c) 2020 Zyn authors and contributors.
 *
 * This file is part of the *Zyn* project.
 *
 * Zyn is a free software project. You can redistribute it and/or
 * modify it following the terms of the MIT License.
 *
 * This software project is distributed *as is*, WITHOUT WARRANTY OF ANY
 * KIND; including but not limited to the WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE and NONINFRINGEMENT.
 *
 * You should have received a copy of the MIT License along with
 * *Zyn*. If not, see <http://opensource.org/licenses/MIT>.
 */

//! # zyn::log
//!
//! This module contains everything related to results emitted by Zyn.

/// Configure the logger to emit log messages on the standard error.
///
///
/// # Arguments
///
/// * `path` - Location from where the log messages are emitted from.
/// * `quiet` - Whether to silence any log messages.
/// * `verbosity` - The level of information to be dispatched by the logger.
pub fn stderrlogger(path: &str, quiet: bool, verbosity: usize) {
    stderrlog::new()
        .module(env!("CARGO_PKG_NAME"))
        .module(path)
        .quiet(quiet)
        .timestamp(stderrlog::Timestamp::Millisecond)
        .verbosity(verbosity)
        .init()
        .unwrap();
}
