/* src/rust/ssh/server.rs
 * ======================
 *
 * Copying
 * -------
 *
 * Copyright (c) 2020 Zyn authors and contributors.
 *
 * This file is part of the *Zyn* project.
 *
 * Zyn is a free software project. You can redistribute it and/or
 * modify it following the terms of the MIT License.
 *
 * This software project is distributed *as is*, WITHOUT WARRANTY OF ANY
 * KIND; including but not limited to the WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE and NONINFRINGEMENT.
 *
 * You should have received a copy of the MIT License along with
 * *Zyn*. If not, see <http://opensource.org/licenses/MIT>.
 */
#![warn(missing_docs)]

//! # zyn::ssh::server
//!
//! Definition of the SSH server facilities.

use std::str;
use std::sync::Arc;

use log::*;
use uuid;

use thrussh::{ChannelId, ChannelOpenFailure, Sig, server};
use thrussh_keys::key::PublicKey;

use crate::errors::*;
use crate::data::config::Config;


/// SSH Server.
#[derive(Clone)]
pub struct Server {
    /// Identifier of the running server.
    pub id: uuid::Uuid,
    /// Configuration provided to Zyn.
    config: Arc<Config>,
    /// INET address of the connected client.
    peer_addr: Option<std::net::SocketAddr>,
}

impl Server {
    /// Instanciate a new server to handle the incomming connection.
    ///
    ///
    /// # Arguments
    ///
    /// * `config` - Zyn's configuration.
    /// * `peer_addr` - The IP address and port of the connecting client.
    ///
    ///
    /// # Returns
    ///
    /// This function return a new server ready to handle the incomming
    /// connection.
    pub fn new(config: Arc<Config>, peer_addr: Option<std::net::SocketAddr>) -> Self {
        return Self {
            id: uuid::Uuid::new_v4(),
            config: config,
            peer_addr: peer_addr,
        }
    }
}

/// Close the provided channel from a given session.
///
///
/// # Arguments
///
/// * `channel` - The SSH channel to be closed.
/// * `session` - The SSH session within which the channel must be closed.
fn die(channel: ChannelId, session: &mut server::Session) {
    session.channel_failure(channel);
    session.eof(channel);
    session.close(channel);
    session.flush().unwrap_or_else(|_| {
        debug!("Cloud not flush session.");
    });
}

impl server::Handler for Server {
    type Error = ZynSSHServerError;

    /// The type of authentications.
    type FutureAuth = futures::future::Ready<Result<(Server, server::Auth), Self::Error>>;
    /// The type of future bools returned by some parts of this handler.
    type FutureBool = futures::future::Ready<Result<(Server, server::Session, bool), Self::Error>>;
    /// The type of units returned by some parts of this handler.
    type FutureUnit = futures::future::Ready<Result<(Server, server::Session), Self::Error>>;

    /// Convert an `Auth` to `Self::FutureAuth`.
    ///
    ///
    /// # Arguments
    ///
    /// * `auth` - The authentication value to be converted.
    fn finished_auth(self, auth: server::Auth) -> Self::FutureAuth {
        futures::future::ready(Ok((self, auth)))
    }

    /// Convert a `bool` to `Self::FutureBool`.
    ///
    ///
    /// # Arguments
    ///
    /// * `b` - The boolean value to be converted.
    /// * `session` - The session belonging to the provided boolean value.
    fn finished_bool(
        self,
        b: bool,
        session: server::Session
    ) -> Self::FutureBool {
        futures::future::ready(Ok((self, session, b)))
    }

    /// Produce a `Self::FutureUnit`.
    ///
    ///
    /// # Arguments
    ///
    /// * `session` - The session value to be converted.
    fn finished(self, session: server::Session) -> Self::FutureUnit {
        futures::future::ready(Ok((self, session)))
    }

    /// Reject authentication using the `none` method.
    ///
    ///
    /// # Arguments
    ///
    /// * `user` - Name of the user to be authenticated.
    ///
    ///
    /// # Returns
    ///
    /// The `none` authentication method is not allowed by this server.
    fn auth_none(self, user: &str) -> Self::FutureAuth {
        trace!(
            "{} - Rejecting 'none' authentication for user: {:?}", self.id, user
        );
        self.finished_auth(server::Auth::Reject)
    }

    /// Reject authentication using the `password` method.
    ///
    ///
    /// # Arguments
    ///
    /// * `user` - Name of the user to be authenticated.
    /// * `password` - Password of the user to be authenticated.
    ///
    ///
    /// # Returns
    ///
    /// The `password` authentication method is not allowed by this server.
    #[allow(unused_variables)]
    fn auth_password(self, user: &str, password: &str) -> Self::FutureAuth {
        trace!(
            "{} - Rejecting 'password' authentication for user: {:?}",
            self.id,
            user
        );
        self.finished_auth(server::Auth::Reject)
    }

    /// Reject authentication using the `keyboard-interactive` method.
    ///
    ///
    /// # Arguments
    ///
    /// * `user` - Name of the user to be authenticated.
    /// * `submethods` - The authentication sub-method to be used by both
    ///    parties.
    /// * `response` - The response returned by the client after having received
    ///   the server requests.
    ///
    ///
    /// # Returns
    ///
    /// The `keyboard-interactive` authentication method is not allowed by this
    /// server.
    #[allow(unused_variables)]
    fn auth_keyboard_interactive(
        self,
        user: &str,
        submethods: &str,
        response: Option<server::Response>
    ) -> Self::FutureAuth {
        trace!(
            "{} - Rejecting 'keyboard-interactive' authentication for user: {:?}",
            self.id,
            user
        );
        self.finished_auth(server::Auth::Reject)
    }

    /// Check authentication using the "publickey" method. This method just
    /// checks whether the public key matches the authorized ones. Thrussh is
    /// responsible for the signature check.
    ///
    ///
    /// # Arguments
    ///
    /// * `user` - Name of the user to be authenticated.
    /// * `key` - The public key of the user to be authenticated.
    ///
    ///
    /// # Returns
    ///
    /// Whether or not the user could be successfully authenticated.
    fn auth_publickey(self, user: &str, key: &PublicKey) -> Self::FutureAuth {
        trace!(
            "{} - Authenticating user {:?} with key {:?}.", self.id, user, key
        );

        // TODO: Check provided public key.

        warn!(
            "Public key authentication failed for user {:?} from {}.",
            user, self.peer_addr.unwrap()
        );
        self.finished_auth(server::Auth::Reject)
    }

    /// Reject any attempt to open a `pty` channel.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `term` - Name of the shell terminal to be attached to the session.
    /// * `col_witdh` - Character wise width of the PTY session.
    /// * `row_height` - Character wise height of the PTY session.
    /// * `pix_witdh` - Pixel wise width of the PTY session.
    /// * `pix_height` - Pixel wise height of the PTY session.
    /// * `modes` - The terminal modes to be applied.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// PYT requests are not supported by this server.
    #[allow(unused_variables)]
    fn pty_request(
        self,
        channel: ChannelId,
        term: &str,
        col_width: u32,
        row_height: u32,
        pix_width: u32,
        pix_height: u32,
        modes: &[(thrussh::Pty, u32)],
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!("{} - Failing pty request on {:?}.", self.id, channel);
        die(channel, &mut session);

        self.finished(session)
    }

    /// Reject any attempt to open a `shell` channel.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// Shell requests are not supported by this server.
    fn shell_request(
        self,
        channel: ChannelId,
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!("{} - Failing shell request on {:?}.", self.id, channel);
        die(channel, &mut session);

        self.finished(session)
    }

    /// Reject any attempt to open a `subsystem` channel.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `name` - Name of the subsystem which is requested.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// This server does not support any subsystem.
    fn subsystem_request(
        self,
        channel: ChannelId,
        name: &str,
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!(
            "{} - Failing {} subsystem request on {:?}.", self.id, name, channel
        );
        die(channel, &mut session);

        self.finished(session)
    }

    #[allow(unused_variables)]
    /// Reject any attempt to open a `x11` channel request.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `single_connection` - Whether or not only a single forward request
    ///   should be expected.
    /// * `x11_auth_protocol` - Name of the X11 authentication method to be used.
    /// * `x11_auth_cookie` - Value of the X11 authentication cookie.
    /// * `x11_screen_number` - Number of screens attached to the session.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// X11 requests are not supported by this server.
    fn x11_request(
        self,
        channel: ChannelId,
        single_connection: bool,
        x11_auth_protocol: &str,
        x11_auth_cookie: &str,
        x11_screen_number: u32,
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!("{} - Failing x11 request on {:?}.", self.id, channel);
        die(channel, &mut session);

        self.finished(session)
    }

    /// Reject any attempt to open a `forwarded-tcpip` channel.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `host_to_connect` - Name or IP address of the host to connect to.
    /// * `port_to_connect` - The TCP port to connect to.
    /// * `originator_address` - Name or IP address of the connection originator.
    /// * `originator_port` - The source TCP port of the originating connection.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// TCP/IP forwarding requests are not supported by this server.
    fn channel_open_direct_tcpip(
        self,
        channel: ChannelId,
        host_to_connect: &str,
        port_to_connect: u32,
        originator_address: &str,
        originator_port: u32,
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!(
            "{} - Failing forwarded-tcpip on {:?} for {}:{} -> {}:{}.",
            self.id,
            channel,
            originator_address,
            originator_port,
            host_to_connect,
            port_to_connect
        );

        session.channel_open_failure(
            channel,
            ChannelOpenFailure::UnknownChannelType,
            "Unsupported channel type.",
            "en-US"
        );
        self.finished(session)
    }

    /// Reject any attempt to open a `x11` channel.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `originator_address` - Name or IP address of the connection originator.
    /// * `originator_port` - The source TCP port of the originating connection.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// X11 forwarding requests are not supported by this server.
    fn channel_open_x11(
        self,
        channel: ChannelId,
        originator_address: &str,
        originator_port: u32,
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!(
            "{} - Failing x11 on {:?} from {}:{}.",
            self.id,
            channel,
            originator_address,
            originator_port
        );

        session.channel_open_failure(
            channel,
            ChannelOpenFailure::UnknownChannelType,
            "Unsupported channel type.",
            "en-US"
        );
        self.finished(session)
    }

    /// Handle request to open a new session channel.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// The SSH session handling the current request.
    fn channel_open_session(
        self,
        channel: ChannelId,
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!("{} - Opening session channel {:?}.", self.id, channel);

        // TODO: Handle session. Failing for now.
        session.channel_open_failure(
            channel,
            ChannelOpenFailure::ConnectFailed,
            "Not implemented.",
            "en-US"
        );
        die(channel, &mut session);

        self.finished(session)
    }

    /// Handle data sent by the client.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `data` - The data sent by the client.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// The SSH session handling the current request.
    fn data(
        self,
        channel: ChannelId,
        data: &[u8],
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!("{} - Received data on {:?}: {:?}", self.id, channel, data);

        // TODO: Handle data. Closing for now.
        die(channel, &mut session);

        self.finished(session)
    }

    /// Handle extended data from the client. Extended data originating from the
    /// client is currently ignored.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `code` - Type of the data sent by the client.
    /// * `data` - The data sent by the client.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// The SSH session handling the current request.
    fn extended_data(
        self,
        channel: ChannelId,
        code: u32,
        data: &[u8],
        mut session: server::Session
    ) -> Self::FutureUnit {
        trace!(
            "{} - Received extended data on {:?}: ({}) {:?}",
            self.id,
            channel,
            code,
            data
        );
        die(channel, &mut session);

        self.finished(session)
    }

    /// Handle environment variables sent by the client.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `variable_name` - Name of the environment variable to be set.
    /// * `variable_value` - Value of the environment variable to be set.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// The SSH session handling the current request.
    fn env_request(
        self,
        channel: ChannelId,
        variable_name: &str,
        variable_value: &str,
        mut session: server::Session
    ) -> Self::FutureUnit {
        debug!(
            "{} - Received env request on {:?}: {:?}={:?}.",
            self.id,
            channel,
            variable_name,
            variable_value
        );

        // TODO: Handle variable. Closing for now.
        die(channel, &mut session);

        self.finished(session)
    }

    /// Handle a program execution requested by the client.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `data` - The data to be executed on the server.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// The SSH session handling the current request.
    fn exec_request(
        self,
        channel: ChannelId,
        data: &[u8],
        mut session: server::Session
    ) -> Self::FutureUnit {
        debug!(
            "{} - Received exec request on {:?}: {:?}.",
            self.id,
            channel,
            str::from_utf8(data)
        );

        // TODO: Handle request. Closing for now.
        die(channel, &mut session);

        self.finished(session)
    }

    /// Handle a signal sent by the client.
    ///
    ///
    /// # Arguments
    ///
    /// * `channel` - The channel within which to open the PTY session.
    /// * `signal_name` - Name of the signal sent by the client.
    /// * `session` - The SSH session attached to the request.
    ///
    ///
    /// # Returns
    ///
    /// The SSH session handling the current request.
    fn signal(
        self,
        channel: ChannelId,
        signal_name: Sig,
        mut session: server::Session
    ) -> Self::FutureUnit {
        debug!(
            "{} - Received signal on {:?}: {:?}.",
            self.id,
            channel,
            signal_name
        );

        // TODO: Handle signal. Closing for now.
        die(channel, &mut session);

        self.finished(session)
    }
}
